// Re-set the document title
document.title = 'Manesse Dynamic Fibonacci Sequence in JavaScript';

// Create a red div in the body of the document
var div = document.createElement('div');
div.setAttribute('class', 'red fib-container');
document.querySelector('body').appendChild(div);

/*/ Make a paragraph to hold some instructions
var para = document.createElement('p');
para.textContent = "Write code necessary to";
div.appendChild(para);

var ol = document.createElement('ol');
div.appendChild(ol)

var li = document.createElement('li');
li.textContent = "create a <form>";
ol.appendChild(li);

li = document.createElement('li');
li.textContent = "an <input> of type 'range' with the 'onchange' attribute which calls a JavaScript function";
ol.appendChild(li);

li = document.createElement('li');
li.textContent = "as well as any other functions needed to compute the Fibonacci numbers recursively";
ol.appendChild(li);


li = document.createElement('li');
li.textContent = "and create a <div> for each recursive function call";
ol.appendChild(li);
*/
var form = document.createElement("form");

var slider = document.createElement("input");
slider.setAttribute("type", "range");
slider.setAttribute("min", "0");
slider.setAttribute("max", "11");
slider.setAttribute("oninput", "runFibonacci(this)");
form.appendChild(slider);

div.appendChild(form);

var p = document.createElement("p");
p.setAttribute("id", "showId");
div.appendChild(p);

function show(e) {
    p = document.querySelector("#showId");
    p.textContent = e.value;
}



function runFibonacci(element) {
    var container = document.querySelector("#container");
    if (container) {
        container.remove();
    }
    var div = document.createElement("div");
    div.setAttribute("id", "container");
    fibonacci(parseInt(element.value), div, null, true);
    document.querySelector(".fib-container").appendChild(div);
}


var fibonacci = function (i, div, left, initial) {
    var item = document.createElement("div");
    var p = document.createElement("p");
    item.appendChild(p);
    div.appendChild(item);
    if (left) {
        item.setAttribute("class", "fib-left fib-item");
    } else {
        item.setAttribute("class", "fib-right fib-item");
    }
    if (initial) {
        item.setAttribute("class", "fib-item");
    }
    var number;
    if (i == 0) {
        number = 0;
    } else if (i == 1) {
        number = 1;
    } else {
        number = fibonacci(i - 1, item, true) + fibonacci(i - 2, item, false);
    }
    p.textContent = "fib(" + i + ")=" + number;
    return number;
}